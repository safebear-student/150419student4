package com.safebear.auto.utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Calendar.DATE;
import static org.apache.catalina.manager.ManagerServlet.copy;

public class Utils {
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");
    //  private static final String BROWSER = System.getProperty("browser", "firefox");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);
            case "firefox":
                return new FirefoxDriver(firefoxOptions);
            case "chromeHeadless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);
            case "firefoxHeadless":
                firefoxOptions.addArguments("--headless");
                return new FirefoxDriver(firefoxOptions);

            default:
                return new ChromeDriver(options);

        }


    }

    public static String generateScreenShotFileName() {
        // create filename
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())
                + ".png";
    }

    public static void capturescreenshot(WebDriver driver, String fileName) {

        // Take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        File file = new File("target/screenshots");

        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
            copy(scrFile, new File("target/screenshots/" + fileName));
    }
}







