Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful

  Rules:
  - The user must be informed if the login information is incorrect
  - The user must be informed if the login is successful

  Glossary:
  - User: someone who wants to create a Tools List using our application
  - Supporters: This is what the customer calls 'Admin' users

  Questions:
  - Does the user get locked after a number of invalid login attempts

  @HighRisk
  @HighImpact
  @Regression

  Scenario Outline: A user logs into the application
    Given I navigate to the login page
    When I enter the login details for a '<userType>' and submit
    Then I can see the following message: '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |

